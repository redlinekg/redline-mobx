import { NewModelOptions } from "./interfaces";
export declare const createModel: ({ type, name, endpoint, mixin_types, }: NewModelOptions) => import("mobx-state-tree").IModelType<any, any, any, any>;
//# sourceMappingURL=model_factory.d.ts.map