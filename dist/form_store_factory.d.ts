import { NewFormStoreOptions, PostFormOptions } from "./interfaces";
export declare const createFormStore: ({ name, endpoint }: NewFormStoreOptions) => import("mobx-state-tree").IModelType<{
    status: import("mobx-state-tree").IMaybeNull<import("mobx-state-tree").ISimpleType<string>>;
}, {
    setLoading(): void;
    setError(): void;
    setReady(): void;
    setSending(): void;
} & {
    readonly is_loading: boolean;
    readonly is_sending: boolean;
    readonly is_ready: boolean;
    readonly is_error: boolean;
} & {
    sendOrder(data: any, { axios_params }?: PostFormOptions): Promise<any>;
}, import("mobx-state-tree")._NotCustomized, import("mobx-state-tree")._NotCustomized>;
//# sourceMappingURL=form_store_factory.d.ts.map