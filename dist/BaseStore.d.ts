export declare const BaseStore: import("mobx-state-tree").IModelType<{
    status: import("mobx-state-tree").IMaybeNull<import("mobx-state-tree").ISimpleType<string>>;
}, {
    setLoading(): void;
    setError(): void;
    setReady(): void;
    setSending(): void;
} & {
    readonly is_loading: boolean;
    readonly is_sending: boolean;
    readonly is_ready: boolean;
    readonly is_error: boolean;
}, import("mobx-state-tree")._NotCustomized, import("mobx-state-tree")._NotCustomized>;
//# sourceMappingURL=BaseStore.d.ts.map