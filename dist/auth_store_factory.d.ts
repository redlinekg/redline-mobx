import { NewAuthStoreOptions } from "interfaces";
import { IAnyModelType } from "mobx-state-tree";
export declare const createAuthStore: ({ type, name, me_endpoint, login_endpoint, }: NewAuthStoreOptions) => import("mobx-state-tree").IModelType<{
    status: import("mobx-state-tree").IMaybeNull<import("mobx-state-tree").ISimpleType<string>>;
} & {
    user: import("mobx-state-tree").IMaybeNull<IAnyModelType>;
}, {
    setLoading(): void;
    setError(): void;
    setReady(): void;
    setSending(): void;
} & {
    readonly is_loading: boolean;
    readonly is_sending: boolean;
    readonly is_ready: boolean;
    readonly is_error: boolean;
} & {
    setUser(user_data: any): void;
    resetUser(): void;
    setToken(token: string): void;
    resetToken(): void;
} & {
    getMe(): Promise<void>;
    login(credentials: any): Promise<void>;
    logout(): void;
} & {
    readonly is_authenticated: boolean;
    readonly isAuthenticated: boolean;
}, import("mobx-state-tree")._NotCustomized, import("mobx-state-tree")._NotCustomized>;
//# sourceMappingURL=auth_store_factory.d.ts.map