export declare const BaseModel: import("mobx-state-tree").IModelType<{
    status: import("mobx-state-tree").IMaybeNull<import("mobx-state-tree").ISimpleType<string>>;
} & {
    id: import("mobx-state-tree").IMaybeNull<import("mobx-state-tree").ISimpleType<number>>;
    created_at: import("mobx-state-tree").IMaybeNull<import("mobx-state-tree").ISimpleType<string>>;
    updated_at: import("mobx-state-tree").IMaybeNull<import("mobx-state-tree").ISimpleType<string>>;
}, {
    setLoading(): void;
    setError(): void;
    setReady(): void;
    setSending(): void;
} & {
    readonly is_loading: boolean;
    readonly is_sending: boolean;
    readonly is_ready: boolean;
    readonly is_error: boolean;
}, import("mobx-state-tree")._NotCustomized, import("mobx-state-tree")._NotCustomized>;
//# sourceMappingURL=BaseModel.d.ts.map