import { types, getEnv, hasParent, getParent } from 'mobx-state-tree';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import pick from 'lodash-es/pick';
import urlJoin from 'url-join';
import Cookies from 'browser-cookies';
import { __decorate } from 'tslib';
import deprecated from 'deprecated-decorator';
import { toJS } from 'mobx';

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(n);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _createForOfIteratorHelperLoose(o) {
  var i = 0;

  if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) {
    if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) return function () {
      if (i >= o.length) return {
        done: true
      };
      return {
        done: false,
        value: o[i++]
      };
    };
    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  i = o[Symbol.iterator]();
  return i.next.bind(i);
}

// A type of promise-like that resolves synchronously and supports only one observer

const _iteratorSymbol = /*#__PURE__*/ typeof Symbol !== "undefined" ? (Symbol.iterator || (Symbol.iterator = Symbol("Symbol.iterator"))) : "@@iterator";

const _asyncIteratorSymbol = /*#__PURE__*/ typeof Symbol !== "undefined" ? (Symbol.asyncIterator || (Symbol.asyncIterator = Symbol("Symbol.asyncIterator"))) : "@@asyncIterator";

// Asynchronously call a function and send errors to recovery continuation
function _catch(body, recover) {
	try {
		var result = body();
	} catch(e) {
		return recover(e);
	}
	if (result && result.then) {
		return result.then(void 0, recover);
	}
	return result;
}

var LOADING = "LOADING";
var SENDING = "SENDING";
var READY = "READY";
var ERROR = "ERROR";

var BaseStore = /*#__PURE__*/types.model({
  status: types.maybeNull(types.string)
}).actions(function (self) {
  return {
    setLoading: function setLoading() {
      self.status = LOADING;
    },
    setError: function setError() {
      self.status = ERROR;
    },
    setReady: function setReady() {
      self.status = READY;
    },
    setSending: function setSending() {
      self.status = SENDING;
    }
  };
}).views(function (self) {
  return {
    get is_loading() {
      return self.status === LOADING;
    },

    get is_sending() {
      return self.status === SENDING;
    },

    get is_ready() {
      return self.status === READY;
    },

    get is_error() {
      return self.status === ERROR;
    }

  };
});

var MetaModel = /*#__PURE__*/types.model({
  count: /*#__PURE__*/types.optional(types.number, 0),
  total: /*#__PURE__*/types.optional(types.number, 0),
  page: /*#__PURE__*/types.optional(types.number, 0),
  pageCount: /*#__PURE__*/types.optional(types.number, 0)
});

var createDetailStore = function createDetailStore(_ref) {
  var type = _ref.type,
      endpoint = _ref.endpoint,
      name = _ref.name;
  return types.compose(name, BaseStore, types.model({
    item: types.maybeNull(type)
  })).actions(function (self) {
    return {
      applyData: function applyData(response_data) {
        self.item = type.create(response_data);
      }
    };
  }).actions(function (self) {
    return {
      deleteItem: function deleteItem() {
        self.item = null;
      },
      getOne: function getOne(id, _temp) {
        var _ref2 = _temp === void 0 ? {} : _temp,
            qparams = _ref2.qparams,
            axios_params = _ref2.axios_params;

        try {
          self.setLoading();
          var url = urlJoin(endpoint, id.toString(), qparams ? "?" + RequestQueryBuilder.create(qparams).query() : "");
          return Promise.resolve(_catch(function () {
            return Promise.resolve(getEnv(self).axios.get(url, axios_params)).then(function (response) {
              self.applyData(response.data);
              self.setReady();
            });
          }, function (error) {
            self.setError();
            throw error;
          }));
        } catch (e) {
          return Promise.reject(e);
        }
      },
      create: function create(values) {
        try {
          self.setSending();
          return Promise.resolve(_catch(function () {
            return Promise.resolve(getEnv(self).axios.post(endpoint, values)).then(function (response) {
              self.applyData(response.data);
              self.setReady();
              return self.item;
            });
          }, function (error) {
            self.setError();
            throw error;
          }));
        } catch (e) {
          return Promise.reject(e);
        }
      },
      reset: function reset() {
        self.status = null;
        self.item = null;
      }
    };
  });
};
var createListStore = function createListStore(_ref3) {
  var type = _ref3.type,
      endpoint = _ref3.endpoint,
      name = _ref3.name,
      mixin_types = _ref3.mixin_types;
  return (mixin_types ? types.compose.apply(types, [name, BaseStore, types.model({
    meta: types.optional(MetaModel, {}),
    objects: types.array(type)
  })].concat(mixin_types)) : types.compose(name, BaseStore, types.model({
    meta: types.optional(MetaModel, {}),
    objects: types.array(type)
  }))).actions(function (self) {
    return {
      applyMeta: function applyMeta(data) {
        self.meta = MetaModel.create(data);
      },
      applyObjects: function applyObjects(data) {
        self.objects.replace(data.map(function (d) {
          return type.create(d);
        }));
      },
      appendObjects: function appendObjects(data) {
        var _self$objects;

        (_self$objects = self.objects).push.apply(_self$objects, data.map(function (d) {
          return type.create(d);
        }));
      },
      deleteItem: function deleteItem(item) {
        self.objects.remove(item);
      }
    };
  }).actions(function (self) {
    return {
      applyData: function applyData(response_data, loadmore) {
        if (Array.isArray(response_data)) {
          if (loadmore) {
            self.appendObjects(response_data);
          } else {
            self.applyObjects(response_data);
          }
        } else {
          self.applyMeta(pick(response_data, ["count", "total", "page", "pageCount"]));

          if (loadmore) {
            self.appendObjects(response_data.data);
          } else {
            self.applyObjects(response_data.data);
          }
        }
      }
    };
  }).actions(function (self) {
    return {
      getList: function getList(_temp2) {
        var _ref4 = _temp2 === void 0 ? {} : _temp2,
            _ref4$loadmore = _ref4.loadmore,
            loadmore = _ref4$loadmore === void 0 ? false : _ref4$loadmore,
            qparams = _ref4.qparams,
            axios_params = _ref4.axios_params;

        try {
          self.setLoading();
          return Promise.resolve(_catch(function () {
            var url = urlJoin(endpoint, "?" + RequestQueryBuilder.create(_extends({}, qparams, {}, loadmore ? {
              page: self.meta.page + 1
            } : {})).query());
            return Promise.resolve(getEnv(self).axios.get(url, axios_params)).then(function (response) {
              self.applyData(response.data, loadmore);
              self.setReady();
            });
          }, function (error) {
            self.setError();
            throw error;
          }));
        } catch (e) {
          return Promise.reject(e);
        }
      },
      reset: function reset() {
        self.status = null;
        self.objects.clear();
        self.meta = MetaModel.create();
      }
    };
  });
};

var BaseModel = /*#__PURE__*/types.compose(BaseStore, /*#__PURE__*/types.model({
  id: /*#__PURE__*/types.maybeNull(types.number),
  created_at: /*#__PURE__*/types.maybeNull(types.string),
  updated_at: /*#__PURE__*/types.maybeNull(types.string)
}));

var createModelViews = function createModelViews(self) {
  return {
    getParentStore: function getParentStore() {
      var parent_store = null;

      if (hasParent(self, 1)) {
        parent_store = getParent(self, 1);
      }

      if (!parent_store || !parent_store.setSending) {
        if (hasParent(self, 2)) {
          parent_store = getParent(self, 2);
        }
      }

      return parent_store;
    }
  };
};

var createModelActions = function createModelActions(self, endpoint) {
  return {
    "delete": function _delete() {
      try {
        var parent_store = self.getParentStore();

        if (!parent_store) {
          console.warn("Error on trying to delete item without parent");
        } else if (!parent_store.deleteItem) {
          console.warn("You must to define method deleteItem in parent; parent: " + parent_store);
        }

        if (parent_store) {
          parent_store.setSending();
        }

        var url = urlJoin(endpoint, self.id.toString());
        return Promise.resolve(_catch(function () {
          return Promise.resolve(getEnv(self).axios["delete"](url)).then(function () {
            if (parent_store) {
              if (parent_store.deleteItem) {
                parent_store.deleteItem(self);
              }

              parent_store.setReady();
            }

            return self;
          });
        }, function (error) {
          if (parent_store) {
            parent_store.setError();
          }

          throw error;
        }));
      } catch (e) {
        return Promise.reject(e);
      }
    },
    update: function update(values) {
      try {
        var parent_store = self.getParentStore();
        parent_store.setSending();
        var url = urlJoin(endpoint, self.id.toString());
        return Promise.resolve(_catch(function () {
          return Promise.resolve(getEnv(self).axios.patch(url, values)).then(function () {
            parent_store.setReady();
            return self;
          });
        }, function (error) {
          parent_store.setError();
          throw error;
        }));
      } catch (e) {
        return Promise.reject(e);
      }
    }
  };
};

var createModel = function createModel(_ref) {
  var type = _ref.type,
      name = _ref.name,
      endpoint = _ref.endpoint,
      mixin_types = _ref.mixin_types;
  return (mixin_types ? types.compose.apply(types, [name, BaseModel, type].concat(mixin_types)) : types.compose(name, BaseModel, type)).actions(function (self) {
    return _extends({}, createModelActions(self, endpoint));
  }).views(function (self) {
    return _extends({}, createModelViews(self));
  });
};

var createFormStore = function createFormStore(_ref) {
  var name = _ref.name,
      endpoint = _ref.endpoint;
  return types.compose(name, BaseStore, types.model()).actions(function (self) {
    return {
      sendOrder: function sendOrder(data, _temp) {
        var _ref2 = _temp === void 0 ? {} : _temp,
            _ref2$axios_params = _ref2.axios_params,
            axios_params = _ref2$axios_params === void 0 ? undefined : _ref2$axios_params;

        try {
          self.setSending();
          console.log("new form request with parameters:", axios_params);
          return Promise.resolve(_catch(function () {
            return Promise.resolve(getEnv(self).axios.post(endpoint, data, axios_params)).then(function (response) {
              self.setReady();
              return response.data;
            });
          }, function (error) {
            self.setError();
            throw error;
          }));
        } catch (e) {
          return Promise.reject(e);
        }
      }
    };
  });
};

var createAuthStore = function createAuthStore(_ref) {
  var type = _ref.type,
      name = _ref.name,
      me_endpoint = _ref.me_endpoint,
      login_endpoint = _ref.login_endpoint;
  return types.compose(name, BaseStore, types.model({
    user: types.maybeNull(type)
  })).actions(function (self) {
    return {
      setUser: function setUser(user_data) {
        if (self.user) {
          Object.assign(self.user, user_data);
        } else {
          self.user = type.create(user_data);
        }
      },
      resetUser: function resetUser() {
        self.user = null;
      },
      setToken: function setToken(token) {
        Cookies.set("token", token);
      },
      resetToken: function resetToken() {
        Cookies.erase("token");
      }
    };
  }).actions(function (self) {
    return {
      getMe: function getMe() {
        try {
          if (self.is_loading) return Promise.resolve();

          var _getEnv = getEnv(self),
              logger = _getEnv.logger,
              axios = _getEnv.axios;

          self.setLoading();
          return Promise.resolve(_catch(function () {
            return Promise.resolve(axios.get(me_endpoint)).then(function (response) {
              self.setUser(response.data);
              self.setReady();
            });
          }, function (error) {
            logger.error("Cant to authorize(getMe)", error);
            self.setError();
            throw error;
          }));
        } catch (e) {
          return Promise.reject(e);
        }
      },
      login: function login(credentials) {
        try {
          if (self.is_sending) return Promise.resolve();

          var _getEnv2 = getEnv(self),
              logger = _getEnv2.logger,
              axios = _getEnv2.axios;

          self.setSending();
          return Promise.resolve(_catch(function () {
            return Promise.resolve(axios.post(login_endpoint, credentials)).then(function (response) {
              self.setToken(response.data);
              self.setReady();
            });
          }, function (error) {
            logger.error("Cant to login with credentials", error);
            self.setError();
            throw error;
          }));
        } catch (e) {
          return Promise.reject(e);
        }
      },
      logout: function logout() {
        self.resetToken();
        self.resetUser();
      }
    };
  }).views(function (self) {
    return {
      get is_authenticated() {
        return self.user && self.is_ready;
      },

      get isAuthenticated() {
        console.warn("Method 'isAuthenticated' is deprecated; You must to use getter 'is_authenticated'");
        return self.is_authenticated;
      }

    };
  });
};

var RedlineStores = /*#__PURE__*/function () {
  function RedlineStores(logger, axios, snapshot) {
    this.logger = logger;
    this.axios = axios;
    this.snapshot = snapshot;
    this.stores = {};
  }
  /**
   * Register new Store
   * @param param0
   */


  var _proto = RedlineStores.prototype;

  _proto.add = function add(_ref) {
    var name = _ref.name,
        _ref$snapshot = _ref.snapshot,
        snapshot = _ref$snapshot === void 0 ? {} : _ref$snapshot,
        type = _ref.type,
        envs = _ref.additional_envs;

    if (this.stores[name]) {
      throw new Error("You already add store with name [" + name + "]");
    }

    this.stores[name] = type.create(this.snapshot[name] || snapshot, _extends({
      logger: this.logger,
      axios: this.axios
    }, envs || {}));
  }
  /**
   * Get some store by name
   * @param name
   */
  ;

  _proto.getStore = function getStore(name) {
    return this.stores[name];
  }
  /**
   * Get all stores
   */
  ;

  _proto.getStores = function getStores() {
    return this.stores;
  };

  _proto.getJS = function getJS() {
    return this.toJS();
  }
  /**
   * Get js objects(snapshots) of stores
   */
  ;

  _proto.toJS = function toJS$1() {
    var result = {};

    for (var key in this.stores) {
      result[key] = toJS(this.stores[key]);
    }

    return result;
  };

  _proto.toJSON = function toJSON() {
    return JSON.stringify(this.toJS());
  };

  return RedlineStores;
}();

__decorate([deprecated("toJS")], RedlineStores.prototype, "getJS", null);

var transformToTreeSelectVariants = function transformToTreeSelectVariants(options) {
  var objects = options.objects,
      _options$children_fie = options.children_field,
      children_field = _options$children_fie === void 0 ? "children" : _options$children_fie,
      _options$excludeFn = options.excludeFn,
      excludeFn = _options$excludeFn === void 0 ? function (v) {
    return v;
  } : _options$excludeFn;
  return objects.filter(excludeFn).map(function (object) {
    return {
      label: object.title,
      value: object,
      children: object[children_field] && object[children_field].length ? transformToTreeSelectVariants(_extends({}, options, {
        objects: object[children_field]
      })) : []
    };
  });
};
var translateField = function translateField(type, field_name, languages) {
  var _fields;

  var fields = (_fields = {}, _fields[field_name] = type, _fields);

  for (var _iterator = _createForOfIteratorHelperLoose(languages), _step; !(_step = _iterator()).done;) {
    var lang = _step.value;
    fields[field_name + "__" + lang] = types.maybeNull(type);
  }

  return fields;
};

export { BaseModel, BaseStore, ERROR, LOADING, READY, RedlineStores, SENDING, createAuthStore, createDetailStore, createFormStore, createListStore, createModel, transformToTreeSelectVariants, translateField };
//# sourceMappingURL=redline-mobx.esm.js.map
