import { AxiosInstance } from "axios";
import { IAnyModelType } from "mobx-state-tree";
import { Logger } from "./interfaces";
export interface StoreItem {
    name: string;
    type: IAnyModelType;
    snapshot?: {};
    additional_envs?: {};
}
declare type StoresStore = {
    [key: string]: IAnyModelType;
};
declare type StoresJS = {
    [key: string]: any;
};
export declare class RedlineStores {
    private logger;
    private axios;
    private snapshot;
    stores: StoresStore;
    constructor(logger: Logger, axios: AxiosInstance, snapshot: {
        [key: string]: Record<string, any>;
    });
    /**
     * Register new Store
     * @param param0
     */
    add({ name, snapshot, type, additional_envs: envs }: StoreItem): void;
    /**
     * Get some store by name
     * @param name
     */
    getStore(name: string): IAnyModelType;
    /**
     * Get all stores
     */
    getStores(): StoresStore;
    getJS(): StoresJS;
    /**
     * Get js objects(snapshots) of stores
     */
    toJS(): StoresJS;
    toJSON(): string;
}
export {};
//# sourceMappingURL=stores.d.ts.map