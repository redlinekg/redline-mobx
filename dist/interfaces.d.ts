import { AxiosRequestConfig } from "axios";
import { IAnyModelType } from "mobx-state-tree";
import { CreateQueryParams } from "@nestjsx/crud-request";
declare type WriteFn = (o: any) => void;
export interface Logger {
    fatal: WriteFn;
    error: WriteFn;
    warn: WriteFn;
    info: WriteFn;
    debug: WriteFn;
    trace: WriteFn;
}
export interface NewStoreOptions {
    type: IAnyModelType;
    name: string;
    endpoint: string;
    mixin_types?: [IAnyModelType, IAnyModelType, IAnyModelType];
}
export declare type IdOrSlug = number | string;
export interface GetOneOptions {
    qparams?: CreateQueryParams;
    axios_params?: AxiosRequestConfig;
}
export interface GetListOptions {
    loadmore?: boolean;
    qparams?: CreateQueryParams;
    axios_params?: AxiosRequestConfig;
}
export interface PostFormOptions {
    axios_params?: AxiosRequestConfig;
}
export interface NewModelOptions {
    type: IAnyModelType;
    /**
     * Additional models for mixin
     */
    mixin_types?: [IAnyModelType, IAnyModelType, IAnyModelType];
    name: string;
    endpoint: string;
}
export interface NewFormStoreOptions {
    name: string;
    endpoint: string;
}
export interface NewAuthStoreOptions {
    type: IAnyModelType;
    name: string;
    login_endpoint: string;
    me_endpoint: string;
}
export {};
//# sourceMappingURL=interfaces.d.ts.map