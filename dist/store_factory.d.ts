import { NewStoreOptions } from "./interfaces";
import { IAnyModelType } from "mobx-state-tree";
export declare const createDetailStore: ({ type, endpoint, name, }: NewStoreOptions) => IAnyModelType;
export declare const createListStore: ({ type, endpoint, name, mixin_types, }: NewStoreOptions) => IAnyModelType;
//# sourceMappingURL=store_factory.d.ts.map