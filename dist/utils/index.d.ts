import { IAnyType } from "mobx-state-tree";
interface TransformTreeOptions {
    objects: Record<string, any>[];
    children_field: string;
    excludeFn?: (a: any) => any;
}
interface TreeSelectVariant {
    label: string;
    value: any;
    children: TreeSelectVariant[];
}
export declare const transformToTreeSelectVariants: (options: TransformTreeOptions) => TreeSelectVariant[];
export declare const translateField: (type: IAnyType, field_name: string, languages: string[]) => {
    [key: string]: IAnyType;
};
export {};
//# sourceMappingURL=index.d.ts.map