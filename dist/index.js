
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./redline-mobx.cjs.production.min.js')
} else {
  module.exports = require('./redline-mobx.cjs.development.js')
}
