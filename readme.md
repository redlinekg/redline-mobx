-   [Install](#Install)
-   [RedlineStores](#RedlineStores)
-   [createModel](#createModel)
-   [createDetailStore](#createDetailStore)
-   [createListStore](#createListStore)

## CHANGES

-   `4.0.1`
    - Используется url-join для конкатенации урлов и `endpoint` может больше на заканчиваться обязательно на `/`
-   `4.0.0`
    -   Теперь все методы у моделей и хранилищ выкидывают ошибки, а не проглатывают их



## Install

Для работы с `redline-mobx` вам надо установить следующие пакеты:

-   `mobx`
-   `mobx-state-tree`
-   `@nestjsx/crud-request`

## `RedlineStores`

Аггрегатор всех хранилищ в вашем проекте, схема работы выглядит следующим образом:

-   Вы создаете модели
-   Создаете хранилища с помощью `createDetailStore` или `createListStore`(_не обязательно_)
-   Создаете аггрегатор хранилищ

    ```js
    import { RedlineStores } from "redline-mobx";
    import { SomeStore } from "./stores";

    const stores = new RedlineStores(logger, axios, snapshots);
    stores.add({
        type: SomeStore,
        name: "some_store",
    });
    ```

### Methods

-   `RedlineStores.add({name: string, snapshots: {}, type, additional_envs })` - метод для добавления новых хранилищ
-   `RedlineStores.getStore(name: string)` - найти хранилище по его имени
-   `RedlineStores.getStores()` - получить все сторы
-   `RedlineStores.toJS()` - Вернуть содержимое(_сторы_) как `js-object`
-   `RedlineStores.toJSON()` - Вернуть содержимое как `json-string`

## createModel

Создается тип для модели, надо это потому, что сейчас схема такая что у модели есть дополнительные методы:

-   `some_model_instance.delete()`
-   `some_model_instance.update(values: any)`

Для того чтобы создать новую модель надо сделать что-то такое:

```js
import { createModel } from "redline-mobx";
import { types } from "mobx-state-tree";

PostModel = createModel({
    name: "PostModel",
    type: types.model({
        title: types.string,
        author: types.string,
    }),
    endpoint: "/api/posts/",
});
```

модели расширяют стандартную модель `BaseModel` имеющую следующие поля(_она примитивная_):

-   `id`
-   `created_at`
-   `updated_at`

## createDetailStore

Создается тип для хранения одной записи какого-то типа.

```typescript
const PageStore = createDetailStore({
    type: PageModel,
    endpoint: urls.PAGES_ENDPOINT,
    name: "PageStore",
});
```

### Methods

-   `DetailStore.reset()` - Сбросить состояние
-   `DetailStore.getOne(id_slug: string, {qparams?: CreateQueryParams, axios_params?: AxiosRequestConfig })` - Сделать запрос на бэк
-   `DetailStore.create(values: any)` - Отправляет запрос на создание новой записи на бэк и из того что бэк вернет попытается получить запись(`item`)

## createListStore

Создается тип для хранения списка записей

```typescript
const PagesStore = createListStore({
    type: PageModel,
    endpoint: urls.PAGES_ENDPOINT,
    name: "PagesStore",
    mixin_types: [SeoBase],
});
```

### Methods

-   `ListStore.reset()` - Сбросить состояние
-   `ListStore.getList({qparams?: CreateQueryParams, axios_params?: AxiosRequestConfig, loadmore?: boolean })` - Сделать запрос на бэк

## createFormStore

Почти бесполезная функция, но всяго удобнее чем каждый раз писать обращение к `axios` из контекста и.т.п

-   `FormStore.sendOrder(data, ...)` - Отправить форму с предоставленными данными(`data`)

> Статусы меняются в `sending` | `error` | `ready`
>
> **ВАЖНО** ошибки при запросе прокидываются выше.

```typescript
const OrderForm = createFormStore({
    name: "OrderForm",
    endpoint: "api/forms/order_form",
});
const order_form = OrderForm.create();
order_form.sendOrder({
    username: "vl",
    product_id: 11,
});
```

## createAuthStore

Создает стору для авторизации, ожидает в ответ от сервера, при успешном логине токен, который будет сохранен в Cookies.

```typescript
const UserModel = types.model("UserModel", {
    id: types.maybeNull(types.number),
    email: types.maybeNull(types.string),
    password: types.maybeNull(types.string),
});

const AuthStore = createAuthStore({
    type: UserModel,
    name: "AuthStore",
    me_endpoint: urls.ME_ENDPOINT, // URL для получения самого пользователя
    login_endpoint: urls.LOGIN_ENDPOINT, // URL для логина
});

AuthStore.getMe();
AuthStore.user; // User object
```

### Methods

-   `AuthStore.getMe()` - Получит пользователя от сервера и сохранит его в поле `user`
-   `AuthStore.login(credentials)` - Делает запрос для осуществления логина, `credentials` передаются как json в теле POST-запроса на `login_endpoint`. Ожидается, что сервер вернет `token`, который будет сохранен в куки.
-   `AuthStore.logout()` - Удалит запис в куках и почистит поле `user` в самом хранилище

#### Additional

-   `AuthStore.setUser()` - Установить объект пользователя или обновить данные о нем
-   `AuthStore.resetUser()` - Удалить текущего юзера (`AuthStore.user`)
-   `AuthStore.setToken()` - Установить токен в куки
-   `AuthStore.resetToken()` - Удалить токен из кук

### Views

-   `AuthStore.isAuthenticated` - Геттер который ожидает статус сторы как `is_ready` и проверяет наличие информации о пользователе в поле `user`

```typescript
// ....
if (auth_store.isAuthenticated) {
    console.log("Я авторизирован!");
}
// ....
```
