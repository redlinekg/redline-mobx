import { types } from "mobx-state-tree";
import * as constances from "./constances";

export const BaseStore = types
    .model({
        status: types.maybeNull(types.string),
    })
    .actions((self) => ({
        setLoading(): void {
            self.status = constances.LOADING;
        },
        setError(): void {
            self.status = constances.ERROR;
        },
        setReady(): void {
            self.status = constances.READY;
        },
        setSending(): void {
            self.status = constances.SENDING;
        },
    }))
    .views((self) => ({
        get is_loading(): boolean {
            return self.status === constances.LOADING;
        },
        get is_sending(): boolean {
            return self.status === constances.SENDING;
        },
        get is_ready(): boolean {
            return self.status === constances.READY;
        },
        get is_error(): boolean {
            return self.status === constances.ERROR;
        },
    }));
