import { AxiosInstance } from "axios";
import deprecated from "deprecated-decorator";
import { toJS } from "mobx";
import { IAnyModelType } from "mobx-state-tree";
import { Logger } from "./interfaces";

export interface StoreItem {
    name: string;
    type: IAnyModelType;
    snapshot?: {};
    additional_envs?: {};
}

type StoresStore = { [key: string]: IAnyModelType };
type StoresJS = { [key: string]: any };

export class RedlineStores {
    public stores: StoresStore = {};

    constructor(
        private logger: Logger,
        private axios: AxiosInstance,
        private snapshot: { [key: string]: Record<string, any> }
    ) {}
    /**
     * Register new Store
     * @param param0
     */
    add({ name, snapshot = {}, type, additional_envs: envs }: StoreItem): void {
        if (this.stores[name]) {
            throw new Error(`You already add store with name [${name}]`);
        }
        this.stores[name] = type.create(this.snapshot[name] || snapshot, {
            logger: this.logger,
            axios: this.axios,
            ...(envs || {}),
        });
    }
    /**
     * Get some store by name
     * @param name
     */
    getStore(name: string): IAnyModelType {
        return this.stores[name];
    }
    /**
     * Get all stores
     */
    getStores(): StoresStore {
        return this.stores;
    }

    @deprecated("toJS")
    getJS(): StoresJS {
        return this.toJS();
    }

    /**
     * Get js objects(snapshots) of stores
     */
    toJS(): StoresJS {
        const result: { [key: string]: any } = {};
        for (const key in this.stores) {
            result[key] = toJS(this.stores[key]);
        }
        return result;
    }

    toJSON(): string {
        return JSON.stringify(this.toJS());
    }
}
