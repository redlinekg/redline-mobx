import {
    GetListOptions,
    GetOneOptions,
    IdOrSlug,
    NewStoreOptions,
} from "./interfaces";
import { IAnyModelType, IAnyType, getEnv, types } from "mobx-state-tree";

import { AxiosInstance } from "axios";
import { BaseStore } from "./BaseStore";
import { MetaModel } from "./models";
import { RequestQueryBuilder } from "@nestjsx/crud-request";
import pick from "lodash/pick";
import urlJoin from "url-join";

export const createDetailStore = ({
    type,
    endpoint,
    name,
}: NewStoreOptions): IAnyModelType =>
    types
        .compose(
            name,
            BaseStore,
            types.model({
                item: types.maybeNull(type),
            })
        )
        .actions((self) => ({
            applyData(response_data: any): void {
                self.item = type.create(response_data);
            },
        }))
        .actions((self) => ({
            deleteItem(): void {
                self.item = null;
            },
            async getOne(
                id: IdOrSlug,
                { qparams, axios_params }: GetOneOptions = {}
            ): Promise<void> {
                self.setLoading();
                const url = urlJoin(
                    endpoint,
                    id.toString(),
                    qparams
                        ? `?${RequestQueryBuilder.create(qparams).query()}`
                        : ""
                );
                try {
                    const response = await getEnv(self).axios.get(
                        url,
                        axios_params
                    );
                    self.applyData(response.data);
                    self.setReady();
                } catch (error) {
                    self.setError();
                    throw error;
                }
            },
            async create(values: {}): Promise<IAnyType | undefined> {
                self.setSending();
                try {
                    const response = await getEnv(self).axios.post(
                        endpoint,
                        values
                    );
                    self.applyData(response.data);
                    self.setReady();
                    return self.item;
                } catch (error) {
                    self.setError();
                    throw error;
                }
            },
            reset(): void {
                self.status = null;
                self.item = null;
            },
        }));

export const createListStore = ({
    type,
    endpoint,
    name,
    mixin_types,
}: NewStoreOptions): IAnyModelType =>
    (mixin_types
        ? types.compose(
              name,
              BaseStore,
              types.model({
                  meta: types.optional(MetaModel, {}),
                  objects: types.array(type),
              }),
              ...mixin_types
          )
        : types.compose(
              name,
              BaseStore,
              types.model({
                  meta: types.optional(MetaModel, {}),
                  objects: types.array(type),
              })
          )
    )
        .actions((self) => ({
            applyMeta(data: {}): void {
                self.meta = MetaModel.create(data);
            },
            applyObjects(data: {}[]): void {
                self.objects.replace(data.map((d) => type.create(d)));
            },
            appendObjects(data: {}[]): void {
                self.objects.push(...data.map((d) => type.create(d)));
            },
            deleteItem(item: {}): void {
                self.objects.remove(item);
            },
        }))
        .actions((self) => ({
            applyData(response_data: any, loadmore: boolean): void {
                if (Array.isArray(response_data)) {
                    if (loadmore) {
                        self.appendObjects(response_data as {}[]);
                    } else {
                        self.applyObjects(response_data as {}[]);
                    }
                } else {
                    self.applyMeta(
                        pick(response_data, [
                            "count",
                            "total",
                            "page",
                            "pageCount",
                        ])
                    );
                    if (loadmore) {
                        self.appendObjects(response_data.data as {}[]);
                    } else {
                        self.applyObjects(response_data.data as {}[]);
                    }
                }
            },
        }))
        .actions((self) => ({
            async getList({
                loadmore = false,
                qparams,
                axios_params,
            }: GetListOptions = {}): Promise<void> {
                self.setLoading();
                try {
                    const url = urlJoin(
                        endpoint,
                        `?${RequestQueryBuilder.create({
                            ...qparams,
                            ...(loadmore ? { page: self.meta.page + 1 } : {}),
                        }).query()}`
                    );
                    const response = await (getEnv(self)
                        .axios as AxiosInstance).get(url, axios_params);
                    self.applyData(response.data, loadmore);
                    self.setReady();
                } catch (error) {
                    self.setError();
                    throw error;
                }
            },
            reset(): void {
                self.status = null;
                self.objects.clear();
                self.meta = MetaModel.create();
            },
        }));
