import { NewAuthStoreOptions } from "interfaces";
import { getEnv, IAnyModelType, types } from "mobx-state-tree";
import { BaseStore } from "./BaseStore";
import Cookies from "browser-cookies";

export const createAuthStore = ({
    type,
    name,
    me_endpoint,
    login_endpoint,
}: NewAuthStoreOptions) => {
    return types
        .compose(
            name,
            BaseStore,
            types.model({
                user: types.maybeNull(type),
            })
        )
        .actions((self) => ({
            setUser(user_data: any): void {
                if (self.user) {
                    Object.assign(self.user, user_data);
                } else {
                    self.user = type.create(user_data);
                }
            },
            resetUser(): void {
                self.user = null;
            },
            setToken(token: string): void {
                Cookies.set("token", token);
            },
            resetToken(): void {
                Cookies.erase("token");
            },
        }))
        .actions((self) => ({
            async getMe(): Promise<void> {
                if (self.is_loading) return;
                const { logger, axios } = getEnv(self);
                self.setLoading();
                try {
                    const response = await axios.get(me_endpoint);
                    self.setUser(response.data);
                    self.setReady();
                } catch (error) {
                    logger.error("Cant to authorize(getMe)", error);
                    self.setError();
                    throw error;
                }
            },
            async login(credentials: any): Promise<void> {
                if (self.is_sending) return;
                const { logger, axios } = getEnv(self);
                self.setSending();
                try {
                    const response = await axios.post(
                        login_endpoint,
                        credentials
                    );
                    self.setToken(response.data);
                    self.setReady();
                } catch (error) {
                    logger.error("Cant to login with credentials", error);
                    self.setError();
                    throw error;
                }
            },
            logout(): void {
                self.resetToken();
                self.resetUser();
            },
        }))
        .views((self) => ({
            get is_authenticated(): boolean {
                return self.user && self.is_ready;
            },
            get isAuthenticated(): boolean {
                console.warn(
                    "Method 'isAuthenticated' is deprecated; You must to use getter 'is_authenticated'"
                );
                return (self as any).is_authenticated;
            },
        }));
};
