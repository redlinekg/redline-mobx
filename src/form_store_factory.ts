import { BaseStore } from "./BaseStore";
import { NewFormStoreOptions, PostFormOptions } from "./interfaces";
import { types, getEnv } from "mobx-state-tree";
import { AxiosInstance } from "axios";

export const createFormStore = ({ name, endpoint }: NewFormStoreOptions) => {
    return types.compose(name, BaseStore, types.model()).actions((self) => ({
        async sendOrder(
            data: any,
            { axios_params = undefined }: PostFormOptions = {}
        ): Promise<any | undefined> {
            self.setSending();
            console.log("new form request with parameters:", axios_params);
            try {
                const response = await (getEnv(self)
                    .axios as AxiosInstance).post(endpoint, data, axios_params);
                self.setReady();
                return response.data;
            } catch (error) {
                self.setError();
                throw error;
            }
        },
    }));
};
