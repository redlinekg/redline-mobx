import { IAnyType, types } from "mobx-state-tree";

interface TransformTreeOptions {
    objects: Record<string, any>[];
    children_field: string;
    excludeFn?: (a: any) => any;
}

interface TreeSelectVariant {
    label: string;
    value: any;
    children: TreeSelectVariant[];
}

export const transformToTreeSelectVariants = (
    options: TransformTreeOptions
): TreeSelectVariant[] => {
    const {
        objects,
        children_field = "children",
        excludeFn = (v: any) => v,
    } = options;
    return objects.filter(excludeFn).map((object) => {
        return {
            label: object.title,
            value: object,
            children:
                object[children_field] && object[children_field].length
                    ? transformToTreeSelectVariants({
                          ...options,
                          objects: object[children_field],
                      })
                    : [],
        };
    });
};

export const translateField = (
    type: IAnyType,
    field_name: string,
    languages: string[]
): { [key: string]: IAnyType } => {
    const fields: { [key: string]: IAnyType } = {
        [field_name]: type,
    };
    for (const lang of languages) {
        fields[`${field_name}__${lang}`] = types.maybeNull(type);
    }
    return fields;
};
