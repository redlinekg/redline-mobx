import { types } from "mobx-state-tree";

export const MetaModel = types.model({
    count: types.optional(types.number, 0),
    total: types.optional(types.number, 0),
    page: types.optional(types.number, 0),
    pageCount: types.optional(types.number, 0),
});
