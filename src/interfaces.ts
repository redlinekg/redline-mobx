import { AxiosRequestConfig } from "axios";
import { IAnyModelType } from "mobx-state-tree";
import { CreateQueryParams } from "@nestjsx/crud-request";
type WriteFn = (o: any) => void;

export interface Logger {
    fatal: WriteFn;
    error: WriteFn;
    warn: WriteFn;
    info: WriteFn;
    debug: WriteFn;
    trace: WriteFn;
}

export interface NewStoreOptions {
    type: IAnyModelType;
    name: string;
    endpoint: string;
    mixin_types?: [IAnyModelType, IAnyModelType, IAnyModelType];
}

export type IdOrSlug = number | string;

// Methods of Stores

export interface GetOneOptions {
    qparams?: CreateQueryParams;
    axios_params?: AxiosRequestConfig;
}

export interface GetListOptions {
    loadmore?: boolean;
    qparams?: CreateQueryParams;
    axios_params?: AxiosRequestConfig;
}

export interface PostFormOptions {
    axios_params?: AxiosRequestConfig;
}

// Models

export interface NewModelOptions {
    type: IAnyModelType;
    /**
     * Additional models for mixin
     */
    mixin_types?: [IAnyModelType, IAnyModelType, IAnyModelType];
    name: string;
    endpoint: string;
}

// Stores

export interface NewFormStoreOptions {
    name: string;
    endpoint: string;
}

export interface NewAuthStoreOptions {
    type: IAnyModelType;
    name: string;
    login_endpoint: string;
    me_endpoint: string;
}
