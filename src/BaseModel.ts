import { types } from "mobx-state-tree";
import { BaseStore } from "./BaseStore";

export const BaseModel = types.compose(
    BaseStore,
    types.model({
        id: types.maybeNull(types.number),
        created_at: types.maybeNull(types.string),
        updated_at: types.maybeNull(types.string),
    })
);
