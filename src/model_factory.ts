import {
    IAnyModelType,
    getEnv,
    getParent,
    hasParent,
    types,
} from "mobx-state-tree";

import { BaseModel } from "./BaseModel";
import { NewModelOptions } from "./interfaces";
import urlJoin from "url-join";

const createModelViews = (
    self: IAnyModelType
): {
    getParentStore(): IAnyModelType;
} => ({
    getParentStore(): any {
        let parent_store = null;
        if (hasParent(self, 1)) {
            parent_store = getParent(self, 1);
        }
        if (!parent_store || !parent_store.setSending) {
            if (hasParent(self, 2)) {
                parent_store = getParent(self, 2);
            }
        }
        return parent_store;
    },
});

const createModelActions = (
    self: IAnyModelType,
    endpoint: string
): {
    delete: () => Promise<IAnyModelType | undefined>;
    update: (values: any) => Promise<IAnyModelType | undefined>;
} => ({
    async delete(): Promise<IAnyModelType | undefined> {
        const parent_store = (self as any).getParentStore();
        if (!parent_store) {
            console.warn("Error on trying to delete item without parent");
        } else if (!parent_store.deleteItem) {
            console.warn(
                `You must to define method deleteItem in parent; parent: ${parent_store}`
            );
        }
        if (parent_store) {
            parent_store.setSending();
        }
        const url = urlJoin(endpoint, (self as any).id.toString());
        try {
            await getEnv(self).axios.delete(url);
            if (parent_store) {
                if (parent_store.deleteItem) {
                    parent_store.deleteItem(self);
                }
                parent_store.setReady();
            }
            return self;
        } catch (error) {
            if (parent_store) {
                parent_store.setError();
            }
            throw error;
        }
    },
    async update(values: any): Promise<IAnyModelType | undefined> {
        const parent_store = (self as any).getParentStore();
        parent_store.setSending();
        const url = urlJoin(endpoint, (self as any).id.toString());
        try {
            await getEnv(self).axios.patch(url, values);
            parent_store.setReady();
            return self;
        } catch (error) {
            parent_store.setError();
            throw error;
        }
    },
});

export const createModel = ({
    type,
    name,
    endpoint,
    mixin_types,
}: NewModelOptions) => {
    return (mixin_types
        ? types.compose(name, BaseModel, type, ...mixin_types)
        : types.compose(name, BaseModel, type)
    )
        .actions((self: any) => ({
            ...createModelActions(self, endpoint),
        }))
        .views((self: any) => ({
            ...createModelViews(self),
        }));
};
